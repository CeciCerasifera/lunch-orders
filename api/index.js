const express = require('express');
const session = require('express-session');
const cors = require('cors');
const bodyParser = require('body-parser');

//Initialitation
const app = express();
require('./database');

//Settings
app.set('port', process.env.PORT || 3000);

//Middlewares
app.use(cors());
app.use(express.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(session({
    secret: 'mysecretapp',
    resave: true,
    saveUninitialized: true
}));

//Routes
app.use(require('./routes/index'));
app.use('/caterers', require('./routes/caterers') );
app.use('/diners', require('./routes/diners') );
app.use('/menus', require('./routes/menus') );


//Server in listenning
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});
