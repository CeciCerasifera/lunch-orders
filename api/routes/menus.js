const express = require('express');
const router = express.Router();
const Menus = require('../models/Menus');

router.get('/', async (req, res) => {
    try {
        const Menus = await Menus.find();
        res.send(Menus);
    } catch (ex) {
        res.send(ex);
    }
});

router.post('/', async (req, res) => {
    try {
        const objMenu = req.body;
        const newMenu = new Menus(objMenu);
        await newMenu.save();
        res.send({status: 'OK', id: newMenu._id});

    } catch (ex) {
        res.send(ex);
    }

});


module.exports = router;