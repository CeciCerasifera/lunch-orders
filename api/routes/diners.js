const express = require('express');
const router = express.Router();
const Diners = require('../models/Diners');

router.get('/', async (req, res) => {
    try {
        const Diners = await Diners.find();
        res.send(Diners);
    } catch (ex) {
        res.send(ex);
    }
});

router.post('/', async (req, res) => {
    try {
        const objDiner = req.body;
        const newDiner = new Diners(objDiner);
        await newDiner.save();
        res.send({status: 'OK', id: newDiner._id});

    } catch (ex) {
        res.send(ex);
    }

});


module.exports = router;