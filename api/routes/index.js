const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.send({body: 'Time for lunch!'});
});

module.exports = router