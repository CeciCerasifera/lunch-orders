const express = require('express');
const router = express.Router();
const Caterers = require('../models/Caterers');

router.get('/', async (req, res) => {
    try {
        const caterers = await Caterers.find();
        res.send(caterers);
    } catch (ex) {
        res.send(ex);
    }
});

router.post('/', async (req, res) => {
    try {
        const objCaterer = req.body;
        const newCaterer = new Caterers(objCaterer);
        await newCaterer.save();
        res.send({status: 'OK', id: newCaterer._id});

    } catch (ex) {
        res.send(ex);
    }

});


module.exports = router;