const mongoose = require("mongoose");
//mongodb://mongo:27017/lunch-time
mongoose
  .connect("mongodb://localhost:27017/lunch-time", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then((db) => console.log("DB is connected"))
  .catch((err) => console.error(err));
