const mongoose = require('mongoose');
const { Schema } = mongoose;

const CatererSchema =  new Schema({
    name   : { type: String, required: true },
    link   : { type: String, require: true },
    phone1 : { type: String, require: true },
    phone2 : { type: String, require: false },

    date_created : { type: Date, default: Date.now}
});

module.exports = mongoose.model('Caterers', CatererSchema);