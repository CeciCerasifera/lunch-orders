const mongoose = require('mongoose');
const { Schema } = mongoose;

const menuSchema =  new Schema({
    name  : { type: String, required: true },
    price : { type: String, require: true },
    paid  : { type: Boolean, requred: true, default: false},

    date_created : { type: Date, default: Date.now}
});

module.exports = mongoose.model('Menus', menuSchema);