const mongoose = require('mongoose');
const { Schema } = mongoose;

const DinerSchema =  new Schema({
    name  : { type: String, required: true },
    email : { type: String, require: true },
    menus : [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Menus"
        }
    ],

    date_created : { type: Date, default: Date.now}
});

module.exports = mongoose.model('Diners', DinerSchema);