const express = require('express');

//Initialitation
const app = express();

//Settings
//app.set('port', process.env.PORT || 4500);
app.set('port', 4500);

//Routes
app.use(express.static('public'));

//Server in listenning
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});