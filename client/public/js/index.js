let caterers;

function main(){
  fetch('http://localhost:3000/caterers').then(function(res){
    return res.json();

  }).then(function(myJson){

    caterers = myJson;
    renderCaterers(caterers);
    setCaterersOnResponsible(caterers);
  
  }).catch(function(e){
    console.log(e);
  });
}

function addOrder(event, element){
  event.preventDefault();
  let inputValues = document.getElementsByClassName('orderFields');
  let tableBody = document.getElementById('orderValues');
  let newOrder = `<tr><td>${inputValues[0].value}</td><td>${inputValues[1].value}</td><td>${inputValues[2].value}</td><td>$${inputValues[3].value}</td></tr>`;
  tableBody.innerHTML += newOrder;

  element.reset();
}

function addResponsible(event, element){
  event.preventDefault();
  let inputValues = document.getElementsByClassName('responsibleFields');
  let tableBody = document.getElementById('responsibleValues');
  let caterer = caterers[inputValues[0].value];
  let newResponsible = `<tr><td>${caterer.name}</td><td>${inputValues[1].value}</td><td>${inputValues[2].value}</td></tr>`;
  tableBody.innerHTML += newResponsible;

  element.reset();
}

function renderCaterers(caterers){
  let caterersList = document.getElementById('caterers-list');
  let newCatererList = "";
  
  for(let i=0; i<caterers.length; i++){
    newCatererList+= `<li><a href="${caterers[i].link}">${caterers[i].name}</a> ${caterers[i].phone1}${typeof caterers[i].phone2 != 'undefined' ? ", " + caterers[i].phone2 : "" }</li>`;
  }

  caterersList.innerHTML = newCatererList;
}

function setCaterersOnResponsible(caterers){
  let caterersList = document.getElementById('caterers-on-responsible');
  let newCatererList = "<option value=''></option>";

  for(let i=0; i<caterers.length; i++){
    newCatererList+= `<option value="${i}">${caterers[i].name}</option>`;
  }

  caterersList.innerHTML = newCatererList;

}